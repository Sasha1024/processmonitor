#ifndef PROCESSWATCHER_H
#define PROCESSWATCHER_H

#include <iostream>
#include <fstream>
#include <memory>
#include <thread>
#include <mutex>
#include <map>
#include <vector>

struct Process {
    int getPid();
    void needStopThread();
    Process(const std::string &xname, const int xpid,std::shared_ptr<std::ofstream> &xptrLogFile,std::shared_ptr<std::mutex> &xptrMutex);
    Process();
    ~Process();
    void startThread();
private:
    volatile bool stopThread;
    std::string name;
    int pid;
    int lastMemSize;
    int currMemSize;
    std::shared_ptr<std::ifstream> ptrProcessMemSizeFile;
    std::shared_ptr<std::ofstream> ptrLogFile;
    std::shared_ptr<std::mutex> ptrMutex;
    void writeLog(const std::string &message);
    void checkFile();
};


class ProcessWatcher {
    typedef std::vector< std::shared_ptr<Process> > ProcessArr;
    std::map< std::string, ProcessArr > processMap;
    std::shared_ptr<std::ofstream> logFile;
    std::shared_ptr<std::mutex> logMutex;
    bool isAlready(const int xpid);
    void findAndPopFinishedProcess();
    bool checkAllProcess();
    void addRunProcess(const std::string &xname, const std::string &xpid,std::shared_ptr<std::ofstream> &xptrLogFile,std::shared_ptr<std::mutex> &xptrMutex);
    void needKillAllChildThreads();
    bool isAllChildTreadsFinished();
public:
    void writeLog(const std::string &message);
    ProcessWatcher(const std::string &logName);
    ~ProcessWatcher();
    bool run();
    void addProcessName(const std::string &name);
};

#endif // PROCESSWATCHER_H
