#include "processwatcher.h"

int main(int argc, char *argv[])
{
    if (argc>1) {
        ProcessWatcher pw(argv[ argc-1 ]);
        for (int n = 1; n < argc-1; n++) {
            pw.addProcessName(argv[ n ]);
        }
        pw.run();
    }
    else {
        std::cout << "arguments is empty!!" << std::endl;
    }

    return 0;
}
