#include "processwatcher.h"
#include <string.h>

#include <unistd.h>
#include <dirent.h>
#include <sstream>
#include <signal.h>
#include <algorithm>
#include <termios.h>
#include <sys/ioctl.h>

const std::string PROC_DIRECTORY = "/proc/";
const int KEY_ESC =27;
const int MEM_STRING_ID =23;

bool isNumeric(const char* ccharptrCharacterList)
{
    for ( ; *ccharptrCharacterList; ccharptrCharacterList++)
        if (*ccharptrCharacterList < '0' || *ccharptrCharacterList > '9')
            return false;

    return true;
}

void Process::writeLog(const std::string &message)
{
    std::lock_guard<std::mutex> lock((*ptrMutex));
    std::cout << message << std::endl;
    if (ptrLogFile->is_open()&&ptrLogFile->good()) {
        (*ptrLogFile) << message << std::endl;
    }
}

Process::Process(const std::string &xname, const int xpid,std::shared_ptr<std::ofstream> &xptrLogFile,std::shared_ptr<std::mutex> &xptrMutex)
    :name(xname),
     pid(xpid),
     lastMemSize(0),
     currMemSize(0),
     ptrLogFile(xptrLogFile),
     ptrMutex(xptrMutex),
     stopThread(false)
{
    std::string path = PROC_DIRECTORY + std::to_string(pid) + "/stat";
    ptrProcessMemSizeFile = std::make_shared<std::ifstream>();
    ptrProcessMemSizeFile->open(path,std::ifstream::in);
}

Process::Process()
    :name(),
     pid(-1),
     lastMemSize(0),
     currMemSize(0),
     ptrProcessMemSizeFile(nullptr),
     ptrLogFile(nullptr),
     ptrMutex(nullptr),
     stopThread(false)
{
}

void Process::checkFile()
{
    writeLog("Process "+ name + "     pid:" + std::to_string(pid) + " This process is started. Began to observe");

    while(!kill(pid, 0 )&&!stopThread) {
        if (ptrProcessMemSizeFile->is_open()&&ptrProcessMemSizeFile->good()) {
            ptrProcessMemSizeFile->clear();
            ptrProcessMemSizeFile->seekg(0, std::ios::beg);
            std::stringstream buffer;
            buffer << ptrProcessMemSizeFile->rdbuf();
            int i=1;
            std::string str;
            while(buffer>>str) {
                if (i==MEM_STRING_ID) {
                    this->lastMemSize = this->currMemSize;
                    this->currMemSize = std::stod( str )/1000000;
                    int sub = this->currMemSize-this->lastMemSize;
                    if (sub>1) {
                        writeLog("Process " + name + "     pid:" + std::to_string(pid) + " Currect mem size: " + " " +std::to_string(this->currMemSize));
                    }
                    break;
                }
                ++i;
            }

        }
        sleep(1);
    }

    ptrProcessMemSizeFile->close();
    std::string s;
    if (stopThread) {
        s = " becouse ESC button down";
    }
    writeLog("Process "+ name + "     pid:" + std::to_string(pid) + " This process is end" + s);
    pid=-1;
}

void Process::startThread()
{
    std::thread tr(&Process::checkFile, this);
    tr.detach();
}

Process::~Process()
{
}

void Process::needStopThread(){
    stopThread=true;
}

int Process::getPid()
{
    return pid;
};

struct FindProcess {
    int findPid;
    FindProcess(const int id) : findPid(id) {}
    bool operator () ( std::shared_ptr<Process>  &p ) const
    {
        return p->getPid() == findPid;
    }
};

bool ProcessWatcher::isAlready(const int xpid)
{
    for (std::map<std::string,ProcessArr>::iterator iterMap=processMap.begin(); iterMap!=processMap.end(); ++iterMap) {
        ProcessArr::iterator iterVec = std::find_if( iterMap->second.begin(), iterMap->second.end(), FindProcess(xpid));
        if (iterVec != iterMap->second.end()) {
            return false;
        }
    }

    return true;
}

bool ProcessWatcher::isAllChildTreadsFinished()
{
    int numWorkingProcess = 0;
    for (std::map<std::string,ProcessArr>::iterator iterMap=processMap.begin(); iterMap!=processMap.end(); ++iterMap) {
        for (ProcessArr::iterator iterVec = iterMap->second.begin(); iterVec != iterMap->second.end();) {
            if ((*iterVec)->getPid()<0) {
                iterVec = iterMap->second.erase(iterVec);
                writeLog("Process " + iterMap->first + "     was pop inside ProcessWatcher becouse ESC button was down");
            }
            else {
                ++iterVec;
            }
        }
        if (!iterMap->second.empty()){
            ++numWorkingProcess;
        }
    }
    if (numWorkingProcess>0){
        return false;
    }
    else{
        return true;
    }
}

void ProcessWatcher::findAndPopFinishedProcess()
{
    for (std::map<std::string,ProcessArr>::iterator iterMap=processMap.begin(); iterMap!=processMap.end(); ++iterMap) {
        for (ProcessArr::iterator iterVec = iterMap->second.begin(); iterVec != iterMap->second.end(); ) {
            if ((*iterVec)->getPid()<0) {
                iterVec = iterMap->second.erase(iterVec);
                writeLog("Process " + iterMap->first + "     was pop inside ProcessWatcher");
            }
            else {
                ++iterVec;
            }
        }
    }
}

void ProcessWatcher::needKillAllChildThreads(){
    for (std::map<std::string,ProcessArr>::iterator iterMap=processMap.begin(); iterMap!=processMap.end(); ++iterMap) {
        for (ProcessArr::iterator iterVec = iterMap->second.begin(); iterVec != iterMap->second.end(); ++iterVec) {
            (*iterVec)->needStopThread();
        }
    }
}

ProcessWatcher::ProcessWatcher(const std::string &logName)
{
    logMutex = std::make_shared<std::mutex>();
    logFile = std::make_shared<std::ofstream>();
    logFile->open(logName,std::ofstream::out|std::ofstream::trunc);
}

ProcessWatcher::~ProcessWatcher()
{
    logFile->close();
}

void ProcessWatcher::addRunProcess(const std::string &xname, const std::string &xpid,std::shared_ptr<std::ofstream> &xptrLogFile,std::shared_ptr<std::mutex> &xptrMutex)
{
    std::map<std::string,ProcessArr>::iterator search = processMap.find(xname);
    if(search != processMap.end()) {
        std::shared_ptr<Process> p = std::make_shared<Process>(xname, std::stoi(xpid), xptrLogFile, xptrMutex);
        p->startThread();
        search->second.push_back(p);
        writeLog("Process " + xname + "     Inside number "+ std::to_string(search->second.size()) );
    }
}

bool ProcessWatcher::checkAllProcess()
{
    struct dirent* dirEntity = nullptr ;
    DIR* dirProc = opendir(PROC_DIRECTORY.c_str()) ;
    if (dirProc == nullptr) {
        writeLog("ERROR: Couldn't open the " + PROC_DIRECTORY + " directory");
        return false;
    }

    findAndPopFinishedProcess();

    while ((dirEntity = readdir(dirProc)) != 0) {
        if (dirEntity->d_type == DT_DIR) {
            if (isNumeric(dirEntity->d_name)&&isAlready(std::stoi(dirEntity->d_name))) {
                std::string path = PROC_DIRECTORY + dirEntity->d_name + "/status";
                std::shared_ptr<std::ifstream> file = std::make_shared<std::ifstream>();
                file->open(path,std::ifstream::in);
                if (file->is_open()) {
                    std::string str;
                    std::getline ((*file),str);
                    addRunProcess(str, dirEntity->d_name,logFile,logMutex);
                }
                else {
                    file->close();
                }
            }
        }

    }
    closedir(dirProc) ;

    return true;
}

void ProcessWatcher::addProcessName(const std::string &name)
{
    std::pair <std::string, ProcessArr> temp;
    temp.first = "Name:	"+name;
    this->processMap.insert(temp);
}

int kbhit(void)
{
    static bool initFlag = false;
    static const int STDIN = 0;

    if (!initFlag) {
        struct termios term;
        tcgetattr(STDIN, &term);
        term.c_lflag &= ~ICANON;
        tcsetattr(STDIN, TCSANOW, &term);
        setbuf(stdin, NULL);
        initFlag = true;
    }

    int nbbytes;
    ioctl(STDIN, FIONREAD, &nbbytes);
    return nbbytes;
}

bool ProcessWatcher::run()
{
    bool runExitAlgorithm = false;
    while(1) {
        if (kbhit() && getchar() == KEY_ESC) {
            runExitAlgorithm = true;
            needKillAllChildThreads();
            writeLog("Esc button was down. Was run exit algorithm");
        }
        if (runExitAlgorithm){
            if (isAllChildTreadsFinished()){
                writeLog("The program completed successfully");
                return true;
            }
        }
        else {
            checkAllProcess();
        }
        usleep(500);
    }
    return false;
}

void ProcessWatcher::writeLog(const std::string &message)
{
    std::lock_guard<std::mutex> lock((*logMutex));
    std::cout << message << std::endl;
    if (logFile->is_open()&&logFile->good()) {
        (*logFile) << message << std::endl;
    }
}

